package 
{
	import flash.display.Bitmap;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	/**
	 * LoopDrum Main Class
	 * @author takumi
	 */
	public class Main extends Sprite 
	{
		private var fps:FpsCounter = new FpsCounter();
		private var text:TextField = new TextField();
		private var bpm:BpmImageDrawer = new BpmImageDrawer();
		private var bgm:BgmManager = new BgmManager();
	
		/**
		 * constructor
		 */
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		/**
		 * initialize
		 * @param	e
		 */
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			{
				//bg
				{
					var bmp:Bitmap = new Resource.IMG_BG as Bitmap;
					addChild(bmp);
				}
				
				//bpm
				//{
					bpm.init(this);
				//}

				//text
				{
					var textFormat:TextFormat = new TextFormat();
					textFormat.size = 12;
					text.autoSize = TextFieldAutoSize.RIGHT;
					text.selectable = false;
					text.defaultTextFormat = textFormat;
					text.x = 780;
					text.y = 570;
					text.textColor = 0xFFFFFF;
					addChild(text);
				}
				//bgm
				{
					bgm.init();
				}
				//event
				{
					addEventListener(Event.ENTER_FRAME, tick);
				}
				
				{
					bgm.play(true);
				}
			}
		}
		
		/**
		 * tick
		 * @param	event
		 */
		private function tick(event:Event):void
		{
			//update
			{
				fps.update();
				bpm.update();
				text.text = fps.toString();
			}
			//draw
			{
			}
		}
		
	}
}