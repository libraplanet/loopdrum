package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	/**
	 * ...
	 * @author takumi
	 */
	public class BpmImageDrawer extends BpmCounter 
	{
		private var bd:BitmapData = new BitmapData(63 * 3, 80, true, 0x00000000);
		private var sp:Sprite = new Sprite();
		private var imgs:Array = new Array();

		public function BpmImageDrawer() 
		{
			//imgs
			{
				for (var i:int = 0; i < Resource.IMG_NUM.length; i++)
				{
					imgs.push(new Resource.IMG_NUM[i] as Bitmap);
				}
			}
			//sp
			{
				sp.x = 75;
				sp.y = 113;
				sp.addChild(new Bitmap(bd));
			}
		}
		/**
		 * 
		 * @param	s
		 */
		public function init(s:Sprite):void
		{
			s.addChild(sp);
		}
		
		/**
		 * 
		 */
		override public function update():void 
		{
			super.update();
			bd.fillRect(new Rectangle(0, 0, bd.width, bd.height), 0x00000000);
			for (var i:int = 0; i < 3; i++)
			{
				const W:int = 48;
				var m:Matrix = new Matrix();
				m.translate((W * 2) - (W * i), 0);
				bd.draw(imgs[getBpmN(i)], m);
			}
		}
	}
}