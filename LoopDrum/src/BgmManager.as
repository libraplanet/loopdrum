package  
{
	import flash.events.Event;
	import flash.events.SampleDataEvent;
	import flash.display.Sprite;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.ByteArray;
	
	import util.WavePlayer;
	
	/**
	 * BGM再生クラス
	 * 区間リピートを無音なしで実現する
	 * @author takumi
	 */
	public class BgmManager 
	{
		public var waveIndex:int;
		private var waveData:Array;
		private var soundObject:Sound;
		private var soundChanel:SoundChannel;
		public var loop:Boolean;
		
		/**
		 * constructor
		 */
		public function BgmManager() 
		{
		}
		
		/**
		 * init
		 */
		public function init():void
		{
			waveData = 
			[
				new WaveStreamContainer(new Resource.SND_BGM_00 as ByteArray),
				new WaveStreamContainer(new Resource.SND_BGM_01 as ByteArray),
			];
			
			soundObject = new Sound();
		}

		/**
		 * play
		 * @param	isLoop
		 */
		public function play(isLoop:Boolean = true):void
		{
			stop();
			loop = isLoop;
			soundObject.addEventListener(SampleDataEvent.SAMPLE_DATA, onPlaying);
			soundChanel = soundObject.play();
			soundChanel.addEventListener(Event.SOUND_COMPLETE, onStop);
		}
		
		/**
		 * 
		 * @param	event
		 */
		private function onPlaying(event:SampleDataEvent):void
		{
			//const MAX_SAMPLE:int = 8192;//2048;
			/**
			 * 一度に書き込めるサンプル数は8192まで
			 * 再生のサンプリングレートは44.1KHｚで固定
			 * そのため、WAVEファイルは半分の22.05KHzを用意
			 * 2回づつ書き込んでサンプリングレートを合わせる
			 * 最後のBGMファイルをリピート再生
			 */
			const MAX_SAMPLE:int = 4096;//2048;
			var i:int;
			for (i = 0; i < MAX_SAMPLE; i++)
			{
				if (!waveData[waveIndex].isWritableSound())
				{
					waveIndex++;
				}
				if (waveIndex < waveData.length)
				{
					waveData[waveIndex].writeSound(event.data);
					waveData[waveIndex].writeSound(event.data);
					waveData[waveIndex].seekNextSampleOffset(loop && (waveIndex >= (waveData.length - 1)));
				}
				else
				{
					break;
				}
			}
			for (; i < MAX_SAMPLE; i++)
			{
				event.data.writeFloat(0);
				event.data.writeFloat(0);
			}
		}

		/**
		 * 
		 */
		private function onStop():void
		{
			stop();
		}
		
		/**
		 * stop
		 */
		public function stop():void
		{
			if (soundChanel != null)
			{
				soundChanel.stop();
				soundChanel.removeEventListener(Event.SOUND_COMPLETE, onStop);
				soundChanel = null;
				soundObject.removeEventListener(SampleDataEvent.SAMPLE_DATA, onPlaying);
				waveIndex = 0;
				loop = false;
			}
		}
	}
}