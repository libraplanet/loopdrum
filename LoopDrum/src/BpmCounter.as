package  
{
	/**
	 * ...
	 * @author takumi
	 */
	public class BpmCounter 
	{
		internal var bpm:int = 0;
		internal var cnt:int = 0;
		
		public function BpmCounter() 
		{			
		}
		
		public function update():void
		{
			if (cnt < 30)
			{
				cnt++;
			}
			else
			{
				cnt = 0;
				if (bpm < 999)
				{
					bpm++;
				}
			}
		}
		
		internal function getBpmN(l:int):int
		{
			var t:int = bpm;
			while (l > 0)
			{
				t /= 10;
				l--;
			}
			return t % 10;
		}
	}
}