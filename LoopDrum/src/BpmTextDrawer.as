package  
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	/**
	 * ...
	 * @author takumi
	 */
	public class BpmTextDrawer extends BpmCounter
	{
		private var text:Array =
		[
			new TextField(),
			new TextField(),
			new TextField(),
		];
		
		/**
		 * 
		 */
		public function BpmTextDrawer()
		{
			var textFormat:TextFormat = new TextFormat();
			textFormat.size = 80;

			for (var i:int; i < text.length; i++)
			{
				var t:TextField = text[i] as TextField;
				t.autoSize = TextFieldAutoSize.LEFT;
				t.selectable = false;
				t.defaultTextFormat = textFormat;
				t.textColor = 0xFDFFC9;
				t.x = 159 - (i * 40);
				t.y = 93
				update();
			}
		}
		
		/**
		 * 
		 * @param	s
		 */
		public function init(s:Sprite):void
		{
			for (var i:int; i < text.length; i++)
			{
				s.addChild(text[i] as TextField);
			}
		}
		
		/**
		 * 
		 */
		override public function update():void 
		{
			super.update();
			for (var i:int; i < text.length; i++)
			{
				(text[i] as TextField).text = getBpmN(i).toString();
			}
		}
	}
}