package  
{
	import flash.utils.getTimer;

	/**
	 * ...
	 * @author takumi
	 */
	public class FpsCounter 
	{
		private var fps:int = 0;
		private var cnt:int = 0;
		private var time:int = 0;
		
		private var sc:Array = ["/", "-", "\\", "|"];
		private var nc:int = 0;
		
		
		/**
		 * constructor
		 */
		public function FpsCounter() 
		{
			time = getTimer();
		}
		
		public function getFps():int
		{
			return fps;
		}
		
		/**
		 * update
		 */
		public function update():void
		{
			cnt++;
			var now:int = getTimer();
			if ((now - time) >= 1000)
			{
				time = now;
				fps = cnt;
				cnt = 0;
				
				nc++;
				nc += sc.length;
				nc %= sc.length;
			}
		}
		public function toString():String
		{
			return getFps() + "fps" + sc[nc].toString();
		}
	}
}