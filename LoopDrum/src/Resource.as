package  
{
	/**
	 * ...
	 * @author takumi
	 */
	public class Resource 
	{
		[Embed(source="../res/bg.png")]
		public static const IMG_BG:Class;
	
		[Embed(source="../res/num_00.png")]
		private static const IMG_NUM_00:Class;
		[Embed(source="../res/num_01.png")]
		private static const IMG_NUM_01:Class;
		[Embed(source="../res/num_02.png")]
		private static const IMG_NUM_02:Class;
		[Embed(source="../res/num_03.png")]
		private static const IMG_NUM_03:Class;
		[Embed(source="../res/num_04.png")]
		private static const IMG_NUM_04:Class;
		[Embed(source="../res/num_05.png")]
		private static const IMG_NUM_05:Class;
		[Embed(source="../res/num_06.png")]
		private static const IMG_NUM_06:Class;
		[Embed(source="../res/num_07.png")]
		private static const IMG_NUM_07:Class;
		[Embed(source="../res/num_08.png")]
		private static const IMG_NUM_08:Class;
		[Embed(source="../res/num_09.png")]
		private static const IMG_NUM_09:Class;
		
		public static const IMG_NUM:Array =
		[
			IMG_NUM_00,
			IMG_NUM_01,
			IMG_NUM_02,
			IMG_NUM_03,
			IMG_NUM_04,
			IMG_NUM_05,
			IMG_NUM_06,
			IMG_NUM_07,
			IMG_NUM_08,
			IMG_NUM_09,
		];
		
		[Embed(source="../res/bgm_00.wav", mimeType="application/octet-stream")]
		public static const SND_BGM_00:Class;
		[Embed(source="../res/bgm_01.wav", mimeType="application/octet-stream")]
		public static const SND_BGM_01:Class;
		
	}

}