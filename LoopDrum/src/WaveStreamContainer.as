package
{
	import flash.utils.ByteArray;
	import util.WavUtil;
	/**
	 * ...
	 * @author takumi
	 */
	public class WaveStreamContainer 
	{
		
		private var waveLength:int;
		private var waveOffset:int;
		private var waveChanelL:Vector.<Number>;
		private var waveChanelR:Vector.<Number>;

		/**
		 * 
		 * @param	data
		 */
		public function WaveStreamContainer(data:ByteArray)
		{
			//load
			{
				var decodeData:Array = WavUtil.decode(data);
				if (decodeData.length != 0)
				{
					waveChanelL = decodeData[0];
					waveChanelR = decodeData[1] || decodeData[0];
				}
			}
			
			//init
			{
				waveLength = waveChanelL.length;
				waveOffset = 0;
			}
		}
		
		/**
		 * 
		 * @param	data
		 * @return
		 */
		public function writeSound(data:ByteArray):void
		{
			data.writeFloat(waveChanelL[waveOffset]);
			data.writeFloat(waveChanelR[waveOffset]);
		}
		
		/**
		 * 
		 * @param	loop
		 */
		public function seekNextSampleOffset(loop:Boolean):void
		{
			waveOffset++;
			if (loop)
			{
				waveOffset %= waveLength;
			}
		}
		
		/**
		 * 
		 * @return
		 */
		public function isWritableSound():Boolean
		{
			return waveOffset < waveLength;
		}
	}
}