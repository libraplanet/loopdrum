package util
{
	import flash.events.Event;
	import flash.events.SampleDataEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.ByteArray;
	
	/**
	 * ...
	 * @author takumi
	 */
	public class WavePlayer 
	{
		private var soundObject:Sound;
		private var soundChanel:SoundChannel;
		private var waveOffset:int;
		private var waveChanelL:Vector.<Number>;
		private var waveChanelR:Vector.<Number>;
		
		/**
		 * 
		 * @param	data
		 */
		public function WavePlayer(data:ByteArray)
		{
			//load
			{
				var decodeData:Array = WavUtil.decode(data);
				if (decodeData.length != 0)
				{
					waveChanelL = decodeData[0];
					waveChanelR = decodeData[0] || decodeData[1];
				}
			}
			//init
			{
				waveOffset = 0;
				soundObject = new Sound();
				soundChanel = null;
			}
		}
		
		/**
		 * play
		 */
		public function play(loop:Boolean = true):void
		{
			soundObject.addEventListener(SampleDataEvent.SAMPLE_DATA, onPlaying);
			soundChanel = soundObject.play();
			soundChanel.addEventListener(Event.SOUND_COMPLETE, onStop);
		}

		/**
		 * playing event
		 * @param	event
		 */
		private function onPlaying(event:SampleDataEvent):void
		{
			const MAX_SAMPLE:int = 8192;//2048;
			var length:int = waveChanelL.length;
			var i:int;
			for (i = 0; i < MAX_SAMPLE; i++)
			{
				if ((!loop) && (waveOffset >= length))
				{
					if (i == 0)
					{
						stop();
					}
					break;
				}
				else
				{
					waveOffset %= length;
					event.data.writeFloat(waveChanelL[waveOffset]);
					event.data.writeFloat(waveChanelR[waveOffset]);
					waveOffset += 1;
				}
			}		
			for (i = 0; i < MAX_SAMPLE; i++)
			{
				event.data.writeFloat(0);
				event.data.writeFloat(0);
			}
		}
		
		/**
		 * stop event
		 */
		private function onStop():void
		{
			stop();
		}
	
		/**
		 * stop
		 */
		public function stop():void
		{
			if (soundChanel != null)
			{
				soundChanel.stop();
				soundChanel.removeEventListener(Event.SOUND_COMPLETE, onStop);
				soundChanel = null;
				soundObject.removeEventListener(SampleDataEvent.SAMPLE_DATA, onPlaying);
			}
		}
	}
}