package util 
{
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	
	/**
	 * WAVEファイルデコードクラス
	 * ソースは以下のサイトより、そのまま流用
	 * http://level0.kayac.com/#!2010/07/play_external_wav_file.php
	 * @author 
	 */
	public class WavUtil
	{
		public function WavUtil() 
		
		/**
		 * WAVフォーマットのファイルをデコードします.
		 * 
		 * @param	binary WAVファイルのバイナリデータ.
		 * @return	チャンネルごとのPCMデータVectorの配列.
		 */
		public static function decode(binary:ByteArray):Array
		{
			//エンディアンをリトルエンディアンに設定
			binary.endian = Endian.LITTLE_ENDIAN;
			
			//RIFFフォーマットかどうか
			binary.position = 0;
			var isRiff:Boolean = Boolean(binary.readUTFBytes(4) == "RIFF");
			
			//WAVフォーマットかどうか
			binary.position = 8;
			var isWav:Boolean = Boolean(binary.readUTFBytes(4) == "WAVE");
			
			//正式なWAVでない場合は空のデータを返す
			if (!isRiff || !isWav) return new Array();
			
			
			//fmtチャンクのバイト数の取得
			binary.position = 16;
			var chunkSize:uint = binary.readUnsignedInt();
			
			//チャンネル数の取得
			binary.position = 22;
			var channels:int = int(binary.readUnsignedShort());
			
			//サンプリングレートの取得
			var samplingrate:uint = uint(binary.readUnsignedInt());
			
			//サンプリングデータあたりのビット数の取得
			binary.position = 34;
			var bits:int = binary.readUnsignedShort();
			
			//波形データのバイト数の取得
			binary.position = 24 + chunkSize;
			var dataSize:uint = binary.readUnsignedInt();
			
			//波形データ取得
			var rawBytes:ByteArray = new ByteArray();
			binary.readBytes(rawBytes, 28 + chunkSize, dataSize);
			
			
			//戻り値用データ格納配列作成
			var data:Array = [ new Vector.< Number >() ];
			if(channels == 2) data.push( new Vector.< Number >() );
			
			//波形データの解析
			var i:int;
			var v:Number;
			rawBytes.position = 0;
			rawBytes.endian = Endian.LITTLE_ENDIAN;
			while (rawBytes.position < rawBytes.length)
			{
				for (i = 0; i < channels; i++) 
				{
					switch(bits)
					{
						case 8: //8bit
							v = (rawBytes.readUnsignedByte() - 128) / 128;
						break;
						
						case 16: //16bit
							v = rawBytes.readShort() / 32768;
						break;
					}
					data[i].push(v);
				}
			}
			return data;
		}
	}
}
